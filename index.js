const express = require('express');
const app = express();
const http = require('http');
const server = http.createServer(app);
const { Server } = require("socket.io");
const io = new Server(server);


// home page api
app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');
});

// listening to a new connection
io.on('connection', (socket) => {
    console.log('a user connected with id: ' + socket.id);
    // join a room
    socket.join('room1');

    // send a message to all clients in the room
    io.to('room1').emit('welcome', 'A new client has joined the room');

    // leave the room
    socket.leave('room1');

    socket.on('chat_message', (msg) => {
        // sending message back to client
        io.emit('chat_message', msg);
    });
    socket.on("disconnect", () => {
        console.log("user disconnected with id: " + socket.id);
    });
});

server.listen(3000, () => {
  console.log('listening on port: 3000');
});